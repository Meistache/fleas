# FLEAS #

### Summary ###

FLEAS stands for _"Free/Libre Enhanced Appliance Server"_. It's a Vagrant box intended to be a lightweight, heavily-tuned and **free as in freedom** Java web application environment.

It's built over Parabola GNU/Linux and uses Jetty as web container. All the software included is licensed under GNU General Public License v3.

### Information ###

Right now, FLEAS is a huge bundled box with enabled LVM + LUKS encrypted home and log partitions already containing companyNews.war application deployed.

Box has a few tools to play with:

* zsh
* git
* ssh
* sudo
* NFS sync between host and guest
* It's mean to be lightweight so it's enough for now

TMI documentation found at [pirigetty.postach.io]()

### Setup ###

Everything you need is to run

`$ vagrant init meistache/FLEAS; vagrant up --provider virtualbox`

However, you can just run `vagrant init` and then edit your `environment.yaml` so you can adjust provisioning parameters. This is a commented sample of it:

+ disk_path: path to create the sidekick disk
+ disk_size: new disk size in GB
+ cpus: number of CPU cores
+ mem_ammt: memory ammount in MB
+ network_ip: network IP which VM will actually use
+ network: network range for future multibox ideas
+ locale: default locale for guest machine
+ sync_host: directory to sync with Vagrant box

#### Testing your app ####

CompanyNews app is already provisioned from [https://bitbucket.org/Meistache/company-news]()

You can access your app on 192.168.10.248:8080/companyNews/

### Can I make/remaster FLEAS on my VirtualBox? ###

Yes you can. Set up your Parabola GNU/Linux ISO and your network connection accordingly and then run:

`# curl https://bitbucket.org/Meistache/entrance/raw/master/fleas.sh | bash`

If you prefer some double-checking or change the script for your needs, just download both fleas.sh and flea-wless.sh to your home directory and run it.

### Known issues ###

- Still working on weird crash while provisioning disk.
- Provisioning script needs some refactoring anyway
- Expanding provision to Docker and AWS