#!/bin/bash
# flea-wless.sh
# Post arch-chroot script for FLEAS Vagrant box
# Don't worry about it, right?
# TODO - Change ugly EOF syntax for sed when it applies

# create users and groups and set root password
groupadd wings
groupadd jetty
useradd -m -d /home/vagrant -s /bin/bash -G wings,jetty vagrant
chmod 750 /home/vagrant
chown vagrant:vagrant /home/vagrant
echo "root:vagrant" | chpasswd -m

# This is ugly, eew.
# But it sets %wings group to sudo (password-wise)
# Then it sets vagrant user to sudo (password-less)
visudo <<EOF
:%s/wheel/wings/g
:s/%wings/vagrant
:%s/# %wings/%wings/g
:wq
EOF

# Set vagrant share, unsecure key and all
INSECURE_KEY="https://raw.githubusercontent.com/mitchellh/vagrant/master/keys/vagrant.pub"
KEY_DIR="/home/vagrant/.ssh"
mkdir ${KEY_DIR}
curl -o ${KEY_DIR}/authorized_keys ${INSECURE_KEY}
chmod 0700 ${KEY_DIR}
chmod 0600 ${KEY_DIR}/authorized_keys
chown vagrant:vagrant ${KEY_DIR}
chown vagrant:vagrant ${KEY_DIR}/authorized_keys

# Enable services
systemctl enable dhcpcd
systemctl enable rcpbind
systemctl enable sshd

# All the small things
mkdir /home/vagrant/shared
chown vagrant:vagrant /home/vagrant/shared
genfstab /mnt > /mnt/etc/fstab
echo -e "en_US.UTF-8 UTF-8\npt_BR.UTF-8 UTF-8" > /etc/locale.gen
locale-gen
echo LANG=en_US > /etc/locale
ln -s /usr/share/zoneinfo/UTC /etc/localtime
sed -i 's/modconf block/modconf block lvm2/g' /etc/mkinitcpio.conf
mkinitcpio -p linux-libre
mkdir /boot/grub
sed -i 's/GRUB_TIMEOUT=5/GRUB_TIMEOUT=0/g' /etc/default/grub
grub-mkconfig -o /boot/grub/grub.cfg
grub-install /dev/sda
