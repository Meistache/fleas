#!/bin/bash
# fleas.sh
# Setup Script for FLEAS Vagrant box
# Keep calm and curl this on your new virtual machine
# based, only based, on mkimage-arch.sh (since it's not Docker)

export LANG="C.UTF-8"

volume_setup() {
  # Setup partition on our base disk
fdisk /dev/sda <<EOF
n
p
1

+200
n
p
2


t
2
8e
a
1
w
EOF
mkfs.ext4 -L system /dev/sda1
mkfs.ext4 -L root /dev/sda2
}

install() {
  # packages to ignore for space savings
  PKGADD="base sudo grub openssh nfs-utils btrfs-progs jre8-openjdk git zsh unzip"
  PKGIGNORE="man-db,man-pages,mdadm,jfsutils,reiserfsprogs,xfsprogs,nano"

  mount /dev/sda2 /mnt
  mkdir /mnt/boot
  mount /dev/sda1 /mnt/boot
  pacstrap /mnt ${PKGADD} --ignore ${PKGIGNORE}
  rm -rf /mnt/var/cache/pacman/pkg/*
}

post_install() {
  POST_INSTALL_URL=https://bitbucket.org/Meistache/entrance/raw/master/flea-wless.sh
  arch-chroot /mnt /bin/bash -c "curl ${POST_INSTALL_URL} | bash"
}

volume_setup
install
post_install
