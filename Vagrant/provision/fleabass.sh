#!/bin/bash
# fleabass.sh
# Provisioning script for FLEAS box
# To be called from Vagrantfile or your own dummy box.

volume_setup() {
# Create LVM on new disk
fdisk /dev/sdb <<EOF
n
p
1


t
8e
w
EOF

pvcreate /dev/sdb1
vgcreate Company /dev/sdb1
lvcreate -L 1G -n swap Company
lvcreate -L 2G -n temp Company
lvcreate -l 40%FREE -n home Company
lvcreate -l 100%FREE -n logs Company
mkswap /dev/Company/swap
swapon /dev/Company/swap
mkfs.ext4 -L temp /dev/Company/temp
}

crypt_setup() {
  mkdir -m 700 /etc/luks-keys
  dd if=/dev/random of=/etc/luks-keys/home bs=1 count=256
  dd if=/dev/random of=/etc/luks-keys/logs bs=1 count=256
  chmod 600 etc/luks-keys/*
  cryptsetup luksFormat -v -c serpent-xts-plain64 -s 512 -h whirlpool /dev/Company/home /etc/luks-keys/home
  cryptsetup luksFormat -v -c serpent-xts-plain64 -s 512 -h whirlpool /dev/Company/logs /etc/luks-keys/home
  cryptsetup -d /etc/luks-keys/home open —-type luks /dev/Company/home homedir
  cryptsetup -d /etc/luks-keys/logs open —-type luks /dev/Company/logs logging
  mkfs.btrfs -L home /dev/mapper/homedir
  mkfs.btrfs -L logging /dev/mapper/logging

  # I'd really want to copy authorized_keys into my new home
  mount /dev/mapper/logging /var/log
  mount /dev/mapper/homedir /mnt
  cp -pr /home/vagrant/* /mnt
  umount /mnt
  mount /dev/mapper/homedir /home/vagrant

}

set_parttabs() {
  # Set crypttab
  echo -e "swp  /dev/Company/swap  /dev/urandom  swap,cipher=aes-xts-plain64:sha256,key-size=256" > /etc/crypttab
  echo -e "tmp  /dev/Company/temp  /dev/urandom  tmp,cipher=aes-xts-plain64:sha256,key-size=256" >> /etc/crypttab
  echo -e "homedir  /dev/Company/home  /etc/luks-keys/home" >> /etc/crypttab
  echo -e "logging  /dev/Company/logs  /etc/luks-keys/logs" >> /etc/crypttab

  echo -e "/dev/sda2\t/\t\text4\t\t\trw,atime,data=ordered\t0 1" > /etc/fstab
  echo -e "/dev/mapper/homedir\t/home\t\tbtrfs\t\t\trw,noatime,space_cache,subvolid=5,subvol=/\t0 0" >> /etc/fstab
  echo -e "/dev/mapper/logging\t/var/log\tbtrfs\t\t\trw,noatime,space_cache,subvolid=5,subvol=/\t0 0" >> /etc/fstab
  echo -e "/dev/sda1\t/boot\text4\t\t\trw,atime,data=ordered\t0 2" >> /etc/fstab
  echo -e "/dev/mapper/swp\t\tswap\t\t\tswapt\t\t\tdefaults\t\t0 0" >> /etc/fstab
  echo -e "/dev/mapper/tmp\t\t/tmp\t\t\ttemps\t\t\trelatime,nodev,nosuid\t0 0" >> /etc/fstab
}

set_tweaks() {
  sysctl -w net.core.rmem_max=25165824
  sysctl -w net.core.wmem_max=8338608
  sysctl -w net.ipv4.tcp_rmem="4096 1887437 25165824"
  sysctl -w net.ipv4.tcp_wmem="1024 8192 8338608"
  sysctl -w kernel.shmmax=4294967296
  sysctl -w vm.nr_hugepages=512
  sysctl -w vm.hugetlb_shm_group=1001
  sysctl -w net.core.somaxconn=9830
  sysctl -w net.ipv4.ip_local_port_range="32768 49155"
  sysctl -w net.ipv4.tcp_tw_recycle=1
  echo -e "@jetty\t\tsoft\tmemlock\t\tunlimited" >> /etc/security/limits.conf
  echo -e "@jetty\t\thard\tmemlock\t\tunlimited" >> /etc/security/limits.conf
  echo -e "@jetty\t\tsoft\tnofile\t\t8192" >> /etc/security/limits.conf
  echo -e "@jetty\t\thard\tnofile\t\t8192" >> /etc/security/limits.conf
}

jetty_install() {
  JETTY_HOME="/usr/local/share/jetty"
  JETTY_BASE="/opt/companyNews"
  JETTY_EXEC="JETTY_HOME=${JETTY_HOME} JETTY_BASE=${JETTY_BASE} /usr/local/bin/jetty"
  VERSION_URL="https://raw.githubusercontent.com/eclipse/jetty.project/jetty-9.3.x/VERSION.txt"
  JETTY_URL="http://repo1.maven.org/maven2/org/eclipse/jetty/jetty-distribution"
  APP_URL="https://bitbucket.org/Meistache/company-news/get/0a9ecdc2318a.zip"
  ZIP_NAME=$(echo $APP_URL | awk -F"/" '{ print $4"-"$5"-"$7 }')

  JETTY_CURRENT=$(curl ${VERSION_URL} | \
  awk -F"-" '$2 ~ "v2016" { print $2 }' | \
  head -n1)
  DL_URL=$(echo -e "${JETTY_URL}/${JETTY_CURRENT%\ }/jetty-distribution-${JETTY_CURRENT%\ }.zip")
  curl -o ~/jetty_stable.zip ${DL_URL}
  unzip ~/jetty_stable.zip -d $(dirname ${JETTY_HOME})
  mv $(dirname ${JETTY_HOME})/jetty-distribution-${JETTY_CURRENT} ${JETTY_HOME}
  mkdir ${JETTY_BASE}
  cp ${JETTY_HOME}/start.ini ${JETTY_BASE}
  curl -o ~/${ZIP_NAME} ${APP_URL}
  unzip ~/${ZIP_NAME} -d ${JETTY_BASE}
  mv ${JETTY_BASE}/${ZIP_NAME%.*} ${JETTY_BASE}/webapps
  chown -R vagrant:jetty ${JETTY_BASE}
  chown -R vagrant:jetty ${JETTY_HOME}
  chmod 770 ${JETTY_HOME}
  source /etc/profile.d/jre.sh
  ln -s ${JETTY_HOME}/bin/jetty.sh /usr/local/bin/jetty
  echo -e "export JETTY_BASE=${JETTY_BASE}" >> /home/vagrant/.bash_profile
  echo -e "export JETTY_HOME=${JETTY_HOME}" >> /home/vagrant/.bash_profile
  su - vagrant -c "${JETTY_EXEC} start --create-files --add-to-start=https,deploy"
}

#volume_setup
#crypt_setup
#set_parttabs
set_tweaks
jetty_install
